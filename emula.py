from math import *
from time import sleep

from graphics import *
Vo=60
t=0
wo=45*3.1415926535/180

y=0
g=9.8

ventana=GraphWin("Simulador",400,400)
ventana.setCoords(0,0,400,400)

while(y>=0):
    x=Vo*cos(wo)*t
    y= Vo*sin(wo)*t-(1.0/2)*g*t*t
    miCirculo=Circle(Point(x,y),10)
    miCirculo.setFill('cyan')
    miCirculo.draw(ventana)
    print(x,y)
    sleep(0.5)
    t= t+0.1

ventana.getMouse()